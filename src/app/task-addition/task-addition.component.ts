import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-task-addition',
  templateUrl: './task-addition.component.html',
  styleUrls: ['./task-addition.component.css']
})
export class TaskAdditionComponent implements OnInit {
  constructor() { }
  items: string[] = []
  item: string;


  addItem() {
    this.items.push(this.item);
    this.item = '';
  }

  ngOnInit(): void {
  }
}
