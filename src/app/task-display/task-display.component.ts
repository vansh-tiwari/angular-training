import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-task-display',
  templateUrl: './task-display.component.html',
  styleUrls: ['./task-display.component.css']
})
export class TaskDisplayComponent implements OnInit {
  constructor() { }
  @Input() itemList: string[];

  deleteItem(i: number) {
    this.itemList.splice(i, 1);
    console.log(this.itemList)
  }

  ngOnInit(): void {
    console.log(this.itemList);
  }
}
